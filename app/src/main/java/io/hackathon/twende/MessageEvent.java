package io.hackathon.twende;

/**
 * Created by nasznjoka on 7/23/16.
 */
public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
