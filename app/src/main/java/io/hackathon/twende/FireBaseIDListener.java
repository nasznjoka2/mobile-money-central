/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.hackathon.twende;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class FireBaseIDListener extends FirebaseInstanceIdService {
    private static final String[] TOPICS = {"global"};
    private static final String TAG = FireBaseListenerService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG,"ID IS REFRESHING "+token);
        try {
            Log.i(TAG, "Registration Token: " + token);
//            sendRegistrationToServer(token);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
    }

    private void sendRegistrationToServer(String token) throws IOException {
        OkHttpClient client = new OkHttpClient();
        HttpUrl url = NetworkProcessing.buildURL("send_gcmtoken");
        RequestBody body = NetworkProcessing.uploadToken(this, token);
        NetworkProcessing.POST(client, url, body);
    }

}
