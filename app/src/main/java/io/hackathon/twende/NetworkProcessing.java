package io.hackathon.twende;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by nasznjoka on 7/23/16.
 */
public class NetworkProcessing {
    static Context context;
    static String TAG = NetworkProcessing.class.getSimpleName();

    public NetworkProcessing() {
    }

    public static JSONObject uploadImage(Bitmap bitmap) {
        String name = String.valueOf(System.currentTimeMillis());
        try {
            File file = Utils.persistImage(context, bitmap, name);
            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

            RequestBody req = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("userid",
                    "8457851245")
                    .addFormDataPart("userfile","profile.png", RequestBody.create(MEDIA_TYPE_PNG,
                            file)).build();

            Request request = new Request.Builder()
                    .url("url")
                    .post(req)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();

            Log.d("response", "uploadImage:"+response.body().string());

            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e(TAG, "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e(TAG, "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }



        //GET network request
        public static String GET(OkHttpClient client, HttpUrl url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }

        //POST network request
        public static String POST(OkHttpClient client, HttpUrl url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }


    //Login request body
    public static RequestBody LoginBody(String username, String password, String token) {
        return new FormBody.Builder()
                .add("action", "login")
                .add("format", "json")
                .add("username", username)
                .add("password", password)
                .add("logintoken", token)
                .build();
    }
    //Login request body
    public static RequestBody uploadToken(Context context, String token) {
        return new FormBody.Builder()
                .add("action", "login")
                .add("format", "json")
                .add("token", token)
                .add("device_imei", Utils.getIMEINO(context))
                .build();
    }

    public static HttpUrl buildURL(String server) {
        return new HttpUrl.Builder()
                .scheme("http") //http
                .host("41.222.176.233:8080/v0.14/MM/accounts/msisdn")
                .addPathSegment(server)//adds "/pathSegment" at the end of hostname
                .addQueryParameter("param1", "value1") //add query parameters to the URL
                .addEncodedQueryParameter("encodedName", "encodedValue")//add encoded query parameters to the URL
                .build();
        /**
         * The return URL:
         *  https://www.somehostname.com/pathSegment?param1=value1&encodedName=encodedValue
         */
    }
    public static MultipartBody uploadRequestBody(String title, String imageFormat, File file) {

        MediaType MEDIA_TYPE = MediaType.parse("image/" + imageFormat); // e.g. "image/png"
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("action", "upload")
                .addFormDataPart("format", "json")
                .addFormDataPart("filename", title + "." + imageFormat) //e.g. title.png --> imageFormat = png
                .addFormDataPart("file", "...", RequestBody.create(MEDIA_TYPE, file))
//                .addFormDataPart("token", token)
                .build();
    }

}
