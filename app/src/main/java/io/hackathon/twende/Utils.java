package io.hackathon.twende;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;

/**
 * Created by nasznjoka on 7/23/16.
 */
public class Utils {

    public static final String AUTHTOKEN = "auth_token";
    public static final String MOBILE = "mobile";

    public Utils() {
    }

    public static final String BASESERVER = "http://41.222.176.233:8080/v0.14/MM/accounts/msisdn/";
    public static final MediaType JSON  = MediaType.parse("application/json; charset=utf-8");
    public static final String CLIENT_DATA = "CLIENT_DATA";
    public static final String APPTYPE = "apptype";
    public static final String CLIENT = "CLIENT";
    public static final String SMSSENT = "sms_sent";
    public static final String PREFLASTLAT = "last_known_lat";
    public static final String ACKITEM = "item";
    public static final String SUCCESS = "success";
    public static final String PREFREGISTER = "show_reregister_key";
    public static final String TAGACK = "acktag";
    public static final String IMEI = "imei";

    protected static File persistImage(Context context, Bitmap bitmap, String name) {
        File filesDir = Environment.getExternalStorageDirectory();
        File imageFile = new File(filesDir, name + ".jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(Utils.class.getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }


    public static boolean checkInteger(String string) {

        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                System.out.println(" this is digit " + string.charAt(i));
                return true;
            } else {
                System.out.println(" this is not digit " + string.charAt(i));
                return false;
            }
        }
        return false;
    }

    public static void sendSMS(Context context,String[] v){
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(v[0], null, v[1], null, null);
            Log.e("MSG",v[1]);
        } catch (Exception ex) {
            Toast.makeText(context.getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
    public static void disableBroadcastReceiver (Context mContext)
    {
        ComponentName receiver = new ComponentName(mContext, SmsReceiver.class);
        PackageManager pm = mContext.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    public static void enableBroadcastReceiver(Context mContext)
    {
        ComponentName receiver = new ComponentName(mContext, SmsReceiver.class);
        PackageManager pm = mContext.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public static String getElapsedTime(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd HH:mm", cal).toString();
        return date;
    }

    public static boolean verifyOtp (Context mContext, String otp)
    {
        if(!otp.isEmpty())
        {
            AppPreference appPreference = new AppPreference(mContext);
            String code = appPreference.getAuthkey();
            if(code.equals(otp))
                return true;
            return false;
        }
        return false;
    }

    public static final String getDeviceId() {
        String m_szDevIDShort = "35" + //we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                Build.USER.length() % 10; //13 digits
        return m_szDevIDShort;
    }

    public static final String generateBatch() {
        String batch = null;

        long number = (long) Math.floor(Math.random() * 900000L) + 100000L;
        batch = String.valueOf(number);
        return batch;
    }

    public static final String getIMEINO(Context ctx) {
        TelephonyManager tManager = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        String imeiid = tManager.getDeviceId().trim();
        if (imeiid.length() > 0 || imeiid.equals("unknown")) {
            imeiid = getDeviceId();
        }
        return imeiid;
    }
}
