/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package io.hackathon.twende;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Created by nasznjoka on 1/2/2015.
 */
public class AppPreference {
	private SharedPreferences _sharedPrefs,_settingsPrefs;
	private SharedPreferences.Editor _prefsEditor;
    Context mContext ;

    public AppPreference(Context context) {
        this._sharedPrefs = context.getSharedPreferences(Utils.CLIENT_DATA, Activity
                .MODE_MULTI_PROCESS);
        this._settingsPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this._prefsEditor = _sharedPrefs.edit();
        mContext = context;
	}


    public boolean getNumberSent(){
        return  _sharedPrefs.getBoolean(Utils.SMSSENT, false);
    }


    public String getNo()
    {
        return _sharedPrefs.getString(Utils.MOBILE, "");
    }


	public void setDeviceRegistered(boolean bool) {
		_prefsEditor.putBoolean(Utils.PREFREGISTER, bool);
		_prefsEditor.commit();
	}


	public boolean isDeviceRegistered() {
		boolean reg = _sharedPrefs.getBoolean(Utils.PREFREGISTER, false);
        return reg;
	}

    public void setNumberSent(){
        _prefsEditor.putBoolean(Utils.SMSSENT, true);
        _prefsEditor.commit();
    }

    public String getApptype()
    {
        return this._sharedPrefs.getString(Utils.APPTYPE, "merchant");
    }

    public String getAuthkey()
    {
        return _sharedPrefs.getString(Utils.AUTHTOKEN, "");
    }

    public void save_no(String vals){
        _prefsEditor.putString(Utils.MOBILE, vals);
        _prefsEditor.commit();
    }
    public void set_apptype(String vals){
        _prefsEditor.putString(Utils.APPTYPE, vals);
        _prefsEditor.commit();
    }

    public void save_phone(String vals){
        _prefsEditor.putString(Utils.MOBILE, vals);
        _prefsEditor.commit();
    }
    public void save_auth(String vals){
        _prefsEditor.putString(Utils.AUTHTOKEN, vals);
        _prefsEditor.commit();
    }
}
