/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package io.hackathon.twende;

import android.os.CountDownTimer;
import android.widget.ProgressBar;

/**
 * @author nasznjoka
 *2014
 * 12:49:39 AM
 */
public class CountItDown extends CountDownTimer{
	ProgressBar progressBar;
		public CountItDown(long millisInFuture, long countDownInterval, ProgressBar progressBar) {
			super(millisInFuture, countDownInterval);
			this.progressBar = progressBar;
		}

		@Override
		public void onTick(long millisUntilFinished) {

			int progress = (int) (millisUntilFinished/1000);
			progressBar.setProgress(progressBar.getMax()-progress);
		}

		@Override
		public void onFinish() {
			this.cancel();
		}

	} 
