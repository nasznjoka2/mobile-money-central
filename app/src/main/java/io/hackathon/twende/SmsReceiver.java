package io.hackathon.twende;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;


/**
 * Created by nasznjoka on 07-05-2016.
 */
public class SmsReceiver extends BroadcastReceiver
{
    private static final String TAG = SmsReceiver.class.getSimpleName();

    @Override
    public void onReceive (Context context, Intent intent)
    {

        final Bundle bundle = intent.getExtras();
        try
        {
            if(bundle != null)
            {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for(Object aPdusObj : pdusObj)
                {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

//                    Log.e(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);
                    AppPreference appPreference = new AppPreference(context);
                    String phn = appPreference.getNo();
                    if (!senderAddress.toLowerCase().contains(phn)) {
                        return;
                    }

                    String verificationCode = getVerificationCode(message);

                    if(verificationCode.equals("000000"))
                        return;

                    MainActivity.getOtpFromSMS(verificationCode);
                }
            }
        }
        catch(Exception e)
        {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Getting the OTP from sms message body
     * ':' is the separator of OTP from the message
     *
     * @param message
     * @return
     */
    private String getVerificationCode (String message)
    {
        message = message.trim();
        if(Utils.checkInteger(message))
            return message;
        return "000000";
    }
}
