package io.hackathon.twende;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindClick;
import io.github.sporklibrary.annotations.BindView;
import okhttp3.HttpUrl;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView ImageView qrCode;
    OkHttpClient client;
    CountItDown countItDown;
    Bitmap bitmap;
    @BindView ProgressBar progressBar;
    @BindView TextView txtupdate;
    static Context mContext;
    @BindView static EditText etno;
    @BindView static EditText etotp;
    @BindView static EditText etamount;
    AppPreference appPreference;
    @BindView static Button btn_submit;
    @BindView static Button btn_confirm;
    @BindView static Button btn_cancel;
    @BindView static Button  btn_retry;
    @BindView static Button  btn_exit;
    @BindView static Button  btn_deposit;
    @BindView ProgressBar progress;

    @BindView static View register_container;
    @BindView static View confirm_container;
    @BindView static View deposit_container;
    String mobile = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Spork.bind(this);
        appPreference = new AppPreference(this);
        client = new OkHttpClient();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mContext = this;
        countItDown = new CountItDown(60000, 1000, progressBar);

        String no = appPreference.getNo();
        if(!Validating.areSet(no) && no.length() < 1){
            register_container.setVisibility(View.VISIBLE);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrCode.setImageBitmap(bitmap);
                File f = Utils.persistImage(MainActivity.this, bitmap, String.valueOf(System
                        .currentTimeMillis()));
                HttpUrl url = NetworkProcessing.buildURL("upload_qrcode");
                MultipartBody body = NetworkProcessing.uploadRequestBody("testing","jpeg",f);
                CountingRequestBody monitoredRequest = new CountingRequestBody(body, new CountingRequestBody.Listener() {
                    @Override
                    public void onRequestProgress(long bytesWritten, long contentLength) {
                        float percentage = 100f * bytesWritten / contentLength;
                        if (percentage >= 0) {
                            String perc = String.valueOf(percentage);
                            progressBar.setProgress(Integer.parseInt(perc));
                        } else {
                            //Something went wrong
                        }
                    }
                });
                try {
                    NetworkProcessing.POST(client, url, monitoredRequest.delegate);
                    progressBar.setVisibility(View.VISIBLE);
                    txtupdate.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSms();
            }
        });

        btn_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = etamount.getText().toString();
                if(Validating.areSet(amount)) {
                    deposit_container.setVisibility(View.GONE);
                    String ph = appPreference.getNo();
                    ph = ph.replace("+","");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("amount",amount);
                        jsonObject.put("receiver",ph);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        bitmap = QRCodeProcessing.generateQRCode(ph);
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    public void verifyMobileNo() throws IOException, JSONException {
        String no = appPreference.getNo();
        no = no.replace("+","");
        OkHttpClient client = new OkHttpClient();

        String response = doGetRequest(Utils.BASESERVER+"/"+no+"/accountName");
        JSONObject jsonObject = new JSONObject(response);
        String fn = jsonObject.getJSONObject("name").getString("fullName");
        showDialog(R.string.strsuccess,fn);
    }

    String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Basic MjU1NzE4NTMyNDMyOjAwMDA=")
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void sendSms(){

        String s = etno.getText().toString().trim();
        if (Validating.areSet(s)) {
            String n = s.substring(0, 1);
            String n1 = s.substring(1, 2);
            etno.setHintTextColor(Color.parseColor("#ff636363"));

            if (!n.equals("0") || n1.equals("0")) {
                etno.setText("");
                etno.setHintTextColor(Color.parseColor("#FA7306"));
                showToast(getString(R.string.strwrongformat));
                return;
            } else if (s.length() != 10) {
                showToast(getString(R.string.strwrongnolenth));
                etno.setText("");
                etno.setHintTextColor(Color.parseColor("#FA7306"));
                return;
            } else {
                s = "+255".concat(s.substring(1));
                etno.setText("");
                etno.setHintTextColor(Color.parseColor("#ff636363"));
//                String code = "123456";
                String code = Utils.generateBatch();
                appPreference.save_auth(code);
                mobile = s;
//                appPreference.save_phone(s);
                Utils.sendSMS(this, new String[]{s, code});
                Utils.enableBroadcastReceiver(mContext);
                register_container.setVisibility(register_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                etotp.setText("");
                confirm_container.setVisibility(confirm_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                countItDown.start();
            }
        }
    }
    public static void getOtpFromSMS (String SMSBody)
    {
        etotp.setText(SMSBody);
        Utils.disableBroadcastReceiver(mContext);
    }


    public static void showToast(String txt) {
        Toast.makeText(mContext, txt, Toast.LENGTH_LONG).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_camera) {
            deposit_container.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_gallery) {
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


//491B1CPD password
    private void showDialog(int title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.strscannow, null);
        builder.show();
    }

    public void scanQRCode() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan(IntentIntegrator.QR_CODE_TYPES);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (result != null) {
            String contents = result.getContents();
            if (contents != null) {
                showDialog(R.string.strsuccess, result.toString());
            } else {
                showDialog(R.string.strfail, getString(R.string.strfailreason));
            }
        }
    }


    @BindClick
    public void btn_confirm(){
        String code = etotp.getText().toString().trim();
        if(Utils.verifyOtp(mContext,code)){
            appPreference.save_no(mobile);
            countItDown.cancel();
            confirm_container.setVisibility(confirm_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            try {
                verifyMobileNo();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else{
            showToast(getString(R.string.strwrongcode));
        }
    }

    @BindClick
    private void btn_retry(){
        int prog = progress.getProgress();
        String msg = String.valueOf(60-prog);
        if(prog < 59) {
            showToast(getString(R.string.strtimerwarn).concat(" ").concat(msg).concat(" ").concat(getString(R.string.strtimerwarn2)));
            return;
        }
        register_container.setVisibility(register_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        confirm_container.setVisibility(confirm_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }
    @BindClick
    private void btn_cancel(){
        register_container.setVisibility(register_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        confirm_container.setVisibility(confirm_container.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        etno.setText("");
    }

}
