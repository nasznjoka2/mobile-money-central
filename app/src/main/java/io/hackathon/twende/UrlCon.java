/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.hackathon.twende;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 *
 * @author nasznjoka
 */
public  class UrlCon {
    static String response = null;
    static JSONObject jObj = null;
    static InputStream is ;
    static String json = "";
    private final String POSTMETHOD ="POST";
    private final String GETMETHOD ="GET";
    public UrlCon() {

    }

    public String getJson(String url,JSONObject params){
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod(POSTMETHOD);
            urlConn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            urlConn.setRequestProperty("Accept", "application/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            Log.e("JSON", response);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return response ;

    }

    public String getJsonOkhttp(String url, JSONObject params){
        final String[] responsedata = {null};
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(Utils.JSON, params.toString());
        final Request request = new Request.Builder().url(url).post(body).build();
        Log.e("OBJECT",body.toString());
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                responsedata[0] = response.body().string();
                Log.e("RESPONSE", responsedata[0]);
            }
        });

        return responsedata[0];
    }

    public JSONObject getJsonObject(String url,  JSONObject params) {

        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod(GETMETHOD);
            urlConn.setRequestProperty("Content-Type", "application/json");
            urlConn.setRequestProperty("Authorization", "Basic MjU1NzE4NTMyNDMyOjAwMDA=");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("JSON", json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String`
        return jObj;

    }

}
